/*
 * Copyright (C) 2018 crDroid Android Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui.invicta.logo;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.systemui.R;
import com.android.systemui.Dependency;
import com.android.systemui.statusbar.policy.DarkIconDispatcher;
import com.android.systemui.tuner.TunerService;

public class LogoImageView extends ImageView implements
        TunerService.Tunable {

    private Context mContext;

    private boolean mAttached;
    private boolean mInvictaLogo;
    private int mInvictaLogoColor;
    private int mInvictaLogoPosition;
    private int mInvictaLogoStyle;
    private int mTintColor = Color.WHITE;

    private static final String STATUS_BAR_INVICTA_LOGO =
            "system:" + Settings.System.STATUS_BAR_INVICTA_LOGO;
    private static final String STATUS_BAR_INVICTA_LOGO_COLOR =
            "system:" + Settings.System.STATUS_BAR_INVICTA_LOGO_COLOR;
    private static final String STATUS_BAR_INVICTA_LOGO_POSITION =
            "system:" + Settings.System.STATUS_BAR_INVICTA_LOGO_POSITION;
    private static final String STATUS_BAR_INVICTA_LOGO_STYLE =
            "system:" + Settings.System.STATUS_BAR_INVICTA_LOGO_STYLE;

    public LogoImageView(Context context) {
        this(context, null);
    }

    public LogoImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LogoImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        final Resources resources = getResources();
        mContext = context;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (mAttached)
            return;

        mAttached = true;

        Dependency.get(DarkIconDispatcher.class).addDarkReceiver(this);

        Dependency.get(TunerService.class).addTunable(this,
                STATUS_BAR_INVICTA_LOGO,
                STATUS_BAR_INVICTA_LOGO_COLOR,
                STATUS_BAR_INVICTA_LOGO_POSITION,
                STATUS_BAR_INVICTA_LOGO_STYLE);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!mAttached)
            return;

        mAttached = false;
        Dependency.get(TunerService.class).removeTunable(this);
        Dependency.get(DarkIconDispatcher.class).removeDarkReceiver(this);
    }

    public void onDarkChanged(Rect area, float darkIntensity, int tint) {
        mTintColor = DarkIconDispatcher.getTint(area, this, tint);
        if (mInvictaLogo && mInvictaLogoPosition == 0 && 
                mInvictaLogoColor == 0xFFFFFFFF) {
            updateInvictaLogo();
        }
    }

    public void updateInvictaLogo() {
        Drawable drawable = null;

        if (!mInvictaLogo || mInvictaLogoPosition == 1) {
            setImageDrawable(null);
            setVisibility(View.GONE);
            return;
        } else {
            setVisibility(View.VISIBLE);
        }

        if (mInvictaLogoStyle == 0) {
            drawable = mContext.getResources().getDrawable(R.drawable.ic_invicta_logo);
        } else if (mInvictaLogoStyle == 1) {
           drawable = mContext.getResources().getDrawable(R.drawable.ic_batfleck);
        } else if (mInvictaLogoStyle == 2) {
            drawable = mContext.getResources().getDrawable(R.drawable.ic_batman_arkham);
        } else if (mInvictaLogoStyle == 3) {
            drawable = mContext.getResources().getDrawable(R.drawable.ic_batman_classic);
        } else if (mInvictaLogoStyle == 4) {
            drawable = mContext.getResources().getDrawable(R.drawable.ic_batmask);
        }

        setImageDrawable(null);

        clearColorFilter();

        if (mInvictaLogoColor == 0xFFFFFFFF) {
            drawable.setTint(mTintColor);
        } else {
            setColorFilter(mInvictaLogoColor, PorterDuff.Mode.SRC_IN);
        }
        setImageDrawable(drawable);
    }

    @Override
    public void onTuningChanged(String key, String newValue) {
        switch (key) {
            case STATUS_BAR_INVICTA_LOGO:
                mInvictaLogo = newValue != null && Integer.parseInt(newValue) == 1;
                break;
            case STATUS_BAR_INVICTA_LOGO_COLOR:
                mInvictaLogoColor =
                        newValue == null ? 0xFFFFFFFF : Integer.parseInt(newValue);
                break;
            case STATUS_BAR_INVICTA_LOGO_POSITION:
                mInvictaLogoPosition =
                        newValue == null ? 0 : Integer.parseInt(newValue);
                break;
            case STATUS_BAR_INVICTA_LOGO_STYLE:
                mInvictaLogoStyle =
                        newValue == null ? 0 : Integer.parseInt(newValue);
                break;
            default:
                break;
        }
        updateInvictaLogo();
    }
}
